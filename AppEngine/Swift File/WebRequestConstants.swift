//
//  WebRequestConstants.swift
//  TimeToSchool
//
//  Created by Sandy on 24/11/17.
//  Copyright © 2017 Kamaraj. All rights reserved.
//

import Foundation

struct WebRqt
{
    func getDataFromServer_methodPOST(url: String, parameter: String, completion: @escaping (_ success: [String : AnyObject]) -> Void)
    {
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        let postString = parameter
        
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { Data, response, error in
            
            guard let data = Data, error == nil else
            {
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print(response!)
                return
            }
            
            let responseString  = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String : AnyObject]
            completion(responseString)
        }
        task.resume()
    }
    
    static func getDataFromServer_using_MethodGET(url: String, completion: @escaping (_ success: [String : AnyObject]) -> Void)
    {
        var request = URLRequest(url: URL(string: url)!)
        
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            guard let data = data, error == nil else
            {
                print("error=\(String(describing: error))")
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {
                print("response = \(String(describing: response))")
            }
            else
            {
                do
                {
                    /*if let array = response.result.value as? [[String: Any]] {
                        //If you want array of task id you can try like
                        let taskArray = array.flatMap { $0["task_id"] as? String }
                        print(taskArray)
                    }*/
                    
                    let responseString  = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                    completion(responseString as! [String : AnyObject])

                }
            }
            
        }
        task.resume()
    }
}

//
//  Common.swift
//  SCM
//
//  Created by web on 28/02/18.
//  Copyright © 2018 SCM. All rights reserved.
//

import Foundation
import MaterialControls
import Reachability
import UIKit

class Common {
    
    static let shared = Common()
    
    var blurView = UIView()
    
    var blurNoRightsView = UIView()

    var lblLoading = UILabel()
    var lblNoRightsText = UILabel()

    var activityIndicatorView = CDActivityIndicatorView()
    var snackbar = TTGSnackbar()
    
    var localDBPath:URL?

    
    typealias AlertConfirmationBlock = (_ index:Int) -> Void
    typealias ActionSheetConfirmationBlock = (_ index:Int) -> Void

    
    
    func showToast(_ msg:String)
    {
        let toast = MDToast()
        toast.text = msg
        toast.show()
    }
    
    func isReachable() -> Bool
    {
        let reachability = Reachability()!
        return reachability.connection != .none
    }
    
    func pushVC(vc:UIViewController, toStoryboardId:String, toVcId:String, animated:Bool)
    {
        
        let story = UIStoryboard.init(name: toStoryboardId, bundle: nil)
        let VC = story.instantiateViewController(withIdentifier: toVcId)
        
        vc.navigationController?.interactivePopGestureRecognizer?.isEnabled = false;
    vc.navigationController?.view.removeGestureRecognizer((vc.navigationController?.interactivePopGestureRecognizer)!)
        
        vc.navigationController?.pushViewController(VC, animated: animated)
        
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func applyFont(lbl:UILabel, fontType:Int, size:CGFloat, color:UIColor)
    {
        
        lbl.textColor = color

        if(fontType == 1)
        {
//            lbl.font = UIFont(name: "OPENSANS-REGULAR", size: size)

//            lbl.font = UIFont.systemFont(ofSize: size, weight: .regular)

        }
        else if(fontType == 2)
        {
//            lbl.font = UIFont(name: "OPENSANS-SEMIBOLD", size: size)

//            lbl.font = UIFont.systemFont(ofSize: size, weight: .semibold)

        }
        
    }
    
    func roundedView(_ view:UIView)
    {
        
        view.layer.cornerRadius = view.frame.size.width/2.0;
        
    }
    
    func roundedShadowView(view:UIView, color:UIColor)
    {
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.shadowRadius = 3.0
        view.layer.shadowOffset = CGSize(width:2.0, height:2.0)
    }
    
    func saveLocal(value:Any, key:String)
    {
        removeLocal(key:key)
        UserDefaults.standard.set(value, forKey: key)
    }

    func getLocal(key:String) -> Any
    {
        
        return UserDefaults.standard.value(forKey: key) ?? ""
    }
    
    func removeLocal(key:String)
    {
        
         UserDefaults.standard.removeObject(forKey:key)
        
    }
    
    func resetLocalStorage()
    {
        
        if let bundleIdentifierBinded = Bundle.main.bundleIdentifier{
            UserDefaults.standard.removePersistentDomain(forName: bundleIdentifierBinded)
        }
        
    }
    
    func txtFieldPadding(_ txt:UITextField)
    {
        
        let paddingView = UIView()
        paddingView.frame = CGRect(x: 0, y: 0, width: 10, height: 20)
        txt.leftView = paddingView
        txt.leftViewMode = .always
        txt.borderStyle = .none
        txt.layer.borderColor = UIColor.lightGray.cgColor;
        txt.layer.borderWidth = 0.8;
        txt.setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        
    }
    
    func txtFieldPaddingLogin(_ txt:UITextField)
    {
        
        let paddingView = UIView()
        paddingView.frame = CGRect(x: 0, y: 0, width: 10, height: 20)
        txt.leftView = paddingView
        txt.leftViewMode = .always
        txt.borderStyle = .none
//        txt.layer.borderColor = UIColor.lightGray.cgColor;
//        txt.layer.borderWidth = 0.8;
        txt.setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        
    }
    
    func btnDropDownDesigns(_ btn:UIButton)
    {
        
        btn.titleEdgeInsets = UIEdgeInsetsMake(0.0, 10.0, 0.0, 30.0)
       
    }
    
    func showOfflineAlert(vc:UIViewController, strTitle:String, msg:String, completionHandler: @escaping AlertConfirmationBlock)
    {
    
    let actionSheetController = UIAlertController (title: strTitle, message: msg, preferredStyle: UIAlertControllerStyle.alert)
    
    actionSheetController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
        
        completionHandler(1)
        
    }))
    
    actionSheetController.addAction(UIAlertAction(title: "RETRY", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
        
            completionHandler(0)
        
    }))
        
    vc.present(actionSheetController, animated: true, completion: nil)
    
    }
    
    func showNetworkConfirmationAlert(vc:UIViewController, strTitle:String, msg:String, completionHandler: @escaping AlertConfirmationBlock)
    {
        
        let actionSheetController = UIAlertController (title: strTitle, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        actionSheetController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            
            completionHandler(1)
        }))
        
        actionSheetController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            
            completionHandler(0)
        }))
        
        vc.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    func showSessionOutLoginAlert(vc:UIViewController, strTitle:String, msg:String, completionHandler: @escaping AlertConfirmationBlock)
    {
        
        let actionSheetController = UIAlertController (title: strTitle, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        actionSheetController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            
            completionHandler(1)
        }))
        
//        actionSheetController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
//
//            completionHandler(0)
//        }))
        
        vc.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    func showSessionAlert(vc:UIViewController, strTitle:String, msg:String)
    {
        
        let actionSheetController = UIAlertController (title: strTitle, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        actionSheetController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            
           // completionHandler(1)
            
           // self.moveLogin(vc)
            
        }))
        
        vc.present(actionSheetController, animated: true, completion: nil)
        
    }

    func showServerConfirmationAlert(vc:UIViewController, strTitle:String, msg:String, completionHandler: @escaping AlertConfirmationBlock)
    {
        
        let actionSheetController = UIAlertController (title: strTitle, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        actionSheetController.addAction(UIAlertAction(title: "RETRY", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            
            completionHandler(1)
            
        }))
        
        actionSheetController.addAction(UIAlertAction(title: "Exit", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            
            exit(0)
           // completionHandler(0)
            
        }))
        
        vc.present(actionSheetController, animated: true, completion: nil)
        
    }
    
//    func moveLogin(_ vc: UIViewController) {
//        //   [self pushVC:vc toStoryboardId:@"Main" toVcId:@"LoginViewController" animated:NO];
////        let story = UIStoryboard(name: "Main", bundle: nil)
////        let toVc: UIViewController? = story.instantiateViewController(withIdentifier: "LoginVC")
////        let nav = UINavigationController(rootViewController: toVc ?? UIViewController())
////        nav.navigationBar.isHidden = true
////        vc.present(nav, animated: false) {() -> Void in }
//
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
//
//        // Create content and menu controllers
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//        let vcContent = story.instantiateViewController(withIdentifier: "LoginVC")
//
//        let storyMenu = UIStoryboard.init(name: "Menu", bundle: nil)
//        let vcLeft = storyMenu.instantiateViewController(withIdentifier: "LeftMenuViewController")
//
//        // Create content and menu controllers
//        let navigationController = UINavigationController(rootViewController: vcContent)
//
//        navigationController.interactivePopGestureRecognizer?.isEnabled = false;
//    navigationController.view.removeGestureRecognizer((navigationController.interactivePopGestureRecognizer)!)
//
//        let leftMenuViewController = vcLeft
//        //   let rightMenuViewController = vcLeft
//
//        // Create side menu controller
//        let sideMenuViewController: AKSideMenu = AKSideMenu(contentViewController: navigationController, leftMenuViewController: leftMenuViewController, rightMenuViewController: nil)
//
//        // sideMenuViewController.backgroundImage = UIImage(named: "Stars")!
//        sideMenuViewController.menuPreferredStatusBarStyle = .lightContent
////        sideMenuViewController.delegate = self
//        sideMenuViewController.contentViewShadowColor = .black
//        sideMenuViewController.contentViewShadowOffset = CGSize(width: 0, height: 0)
//        sideMenuViewController.contentViewShadowOpacity = 0.6
//        sideMenuViewController.contentViewShadowRadius = 15
//        sideMenuViewController.contentViewShadowEnabled = true
//        sideMenuViewController.bouncesHorizontally = false
//        sideMenuViewController.contentViewCornerRadius = 1.0
//
//        sideMenuViewController.panGestureLeftEnabled = false
//        sideMenuViewController.panGestureRightEnabled = false
//
//        appDelegate.window!.rootViewController = sideMenuViewController
//
//        //   self.window!.rootViewController?.view.backgroundColor = .white
//
//        appDelegate.window!.backgroundColor = .white
//        appDelegate.window?.makeKeyAndVisible()
//
//    }

//    func moveDashboard(_ vc: UIViewController) {
//        //   [self pushVC:vc toStoryboardId:@"Main" toVcId:@"LoginViewController" animated:NO];
//        //        let story = UIStoryboard(name: "Main", bundle: nil)
//        //        let toVc: UIViewController? = story.instantiateViewController(withIdentifier: "LoginVC")
//        //        let nav = UINavigationController(rootViewController: toVc ?? UIViewController())
//        //        nav.navigationBar.isHidden = true
//        //        vc.present(nav, animated: false) {() -> Void in }
//
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
//
//        // Create content and menu controllers
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//        let vcContent = story.instantiateViewController(withIdentifier: "DashboardVC")
//
//        let storyMenu = UIStoryboard.init(name: "Menu", bundle: nil)
//        let vcLeft = storyMenu.instantiateViewController(withIdentifier: "LeftMenuViewController")
//
//        // Create content and menu controllers
//        let navigationController = UINavigationController(rootViewController: vcContent)
//
//        let leftMenuViewController = vcLeft
//        //   let rightMenuViewController = vcLeft
//
//        // Create side menu controller
//        let sideMenuViewController: AKSideMenu = AKSideMenu(contentViewController: navigationController, leftMenuViewController: leftMenuViewController, rightMenuViewController: nil)
//
//        // sideMenuViewController.backgroundImage = UIImage(named: "Stars")!
//        sideMenuViewController.menuPreferredStatusBarStyle = .lightContent
//        //        sideMenuViewController.delegate = self
//        sideMenuViewController.contentViewShadowColor = .black
//        sideMenuViewController.contentViewShadowOffset = CGSize(width: 0, height: 0)
//        sideMenuViewController.contentViewShadowOpacity = 0.6
//        sideMenuViewController.contentViewShadowRadius = 15
//        sideMenuViewController.contentViewShadowEnabled = true
//        sideMenuViewController.bouncesHorizontally = false
//        sideMenuViewController.contentViewCornerRadius = 1.0
//
//        sideMenuViewController.panGestureLeftEnabled = false
//        sideMenuViewController.panGestureRightEnabled = false
//
//        appDelegate.window!.rootViewController = sideMenuViewController
//
//        //   self.window!.rootViewController?.view.backgroundColor = .white
//
//        appDelegate.window!.backgroundColor = .white
//        appDelegate.window?.makeKeyAndVisible()
//
//    }
    
    func showActivityIndicator(_ msg: String)
    {
        
        self.hideActivityIndicator()
        
//        GiFHUD.setGif("img_anim.gif")
//
//        GiFHUD.showWithOverlay()
        
        Common.shared.saveLocal(value: "1", key: "IsLoading")
        Common.shared.saveLocal(value: msg, key: "IsLoadingMsg")

        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        if let aWidth = currentWindow?.frame.size.width, let aHeight = currentWindow?.frame.size.height {
            self.blurView = UIView(frame: CGRect(x: 0, y: 0, width: aWidth, height: aHeight))
        }
        self.blurView.backgroundColor = UIColor.black
        self.blurView.alpha = 0.3
        currentWindow?.addSubview(self.blurView)
        self.blurView.isHidden = false
        if let aNamed = UIImage(named: "logo_progress") {
            self.activityIndicatorView = CDActivityIndicatorView(image: aNamed)
        }
        self.activityIndicatorView.center = (currentWindow?.center)!
        currentWindow?.addSubview(self.activityIndicatorView)
        self.lblLoading = UILabel()
        if let aWidth = currentWindow?.frame.size.width {
            self.lblLoading.frame = CGRect(x: 0, y: (currentWindow?.center.y)! + 55.0, width: aWidth, height: 30)
        }
        self.lblLoading.text = msg
        self.lblLoading.textColor = UIColor.white
        self.lblLoading.numberOfLines = 0
        
        self.lblLoading.textAlignment = .center
        self.lblLoading.font = UIFont(name: "Helvetica Bold", size: 16)
        self.lblLoading.isHidden = false
        currentWindow?.addSubview(self.lblLoading)
        self.activityIndicatorView.startAnimating()
            
        

    }

    func hideActivityIndicator()
    {
        
    //    GiFHUD.dismiss()

            self.activityIndicatorView.stopAnimating()
            self.blurView.isHidden = true
            self.lblLoading.isHidden = true
        
        Common.shared.saveLocal(value: "0", key: "IsLoading")

    }
    
    func hideSnackBar() {

        self.snackbar.dismiss()

    }
    
    func showSnackBar(_ msg: String) {
 
        snackbar = TTGSnackbar.init(message: " " + msg, duration:.middle)
        
        snackbar.onTapBlock = { (snackbar) in
            
            DispatchQueue.main.async {
            
                self.snackbar.dismiss()
                
            }
            
        }
        
        let screenRect: CGRect = UIScreen.main.bounds
        let windowWidth = screenRect.size.width
        snackbar.frame = CGRect(x: 0, y: 0, width: windowWidth, height: 64)
        
        // Change the content padding inset
        snackbar.contentInset = UIEdgeInsets.init(top: 8, left: 12, bottom: 8, right: 8)
        
        // Change margin
        snackbar.leftMargin = 0
        snackbar.rightMargin = 0
        snackbar.topMargin = 0
        snackbar.bottomMargin = 0
        snackbar.actionTextNumberOfLines = 2
        snackbar.cornerRadius = 1.0
        
        snackbar.icon = UIImage(named: "ic_logo_snackBar")
        
        snackbar.shouldDismissOnSwipe = true
        
        // Change message text font and color
//        snackbar.messageTextColor = Constants.snackBarTextColor
        snackbar.messageTextFont = UIFont.boldSystemFont(ofSize: 15)
        
        // Change snackbar background color
//        snackbar.backgroundColor = Constants.snackBarBGColor
        // Change animation duration
        snackbar.animationDuration = 0.5
        snackbar.animationType = .slideFromLeftToRight
        
        snackbar.show()
        
        
        
    
        
      /*  let snackbar = MDSnackbar(text: msg, actionTitle: "")
        snackbar.multiline = true
        snackbar.textColor = Constants.snackBarTextColor
        
        snackbar.backgroundColor = Constants.snackBarBGColor
        snackbar.show()*/
        
    }

    func getMobType() -> String {
        return "1"
    }
    
    func getAppType() -> String {
        return "0" // For corp app - 2, For ERP app - 0
    }
    
    func getRS() -> String {
        return "10"
        
    }

    func getHeaders() ->[String:String]
    {
        
        let header = ["":""]
        
        return header
    }
    
    func getUID() -> String {
        return (Common.shared.getLocal(key: "UID") as? String)!
    }
    
    func getPWD() -> String {
        return (Common.shared.getLocal(key: "PWD") as? String)!
    }
    
    func getLoginType() -> String {
        
        let dictResp = Common.shared.getLocal(key: "LoginResponse") as? NSDictionary

        let strLoginType = dictResp?.value(forKey: "Login_Type") as! String

        return strLoginType
    }
    
    func getCreId() -> String {
        
        let dictResp = Common.shared.getLocal(key: "LoginResponse") as? NSDictionary
        
        let strCreId = dictResp?.value(forKey: "cr_id") as! String
        
        return strCreId
    }
    
    func getName() -> String {
        
        let dictResp = Common.shared.getLocal(key: "LoginResponse") as? NSDictionary
        
        let strName = dictResp?.value(forKey: "name") as? String
        
        return strName ?? ""
    }
    
   
    
    func getCurrentDate(format: String) -> String {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let result = formatter.string(from: date)
        return result
    }
    
    func changeDateFormat(strDate: String) -> String
    {
        
        let myDateString = strDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.A"
        let myDate = dateFormatter.date(from: myDateString)!
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let somedateString = dateFormatter.string(from: myDate)
        
        return somedateString
        
    }
    
    func changeDateFormatOrdinary(strDate: String, currentFormat: String) -> String
    {
        
        let myDateString = strDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentFormat
        let myDate = dateFormatter.date(from: myDateString)!
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let somedateString = dateFormatter.string(from: myDate)
        
        return somedateString
        
    }
    
    
    func changeDateFormatOneToOne(strDate: String, currentFormat: String, toFormat: String) -> String
    {
        
        let myDateString = strDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentFormat
        let myDate = dateFormatter.date(from: myDateString)!
        
        let dateFormatterNew = DateFormatter()
        dateFormatterNew.dateFormat = toFormat
        let somedateString = dateFormatterNew.string(from: myDate)
        
        return somedateString
        
    }
    
   
  
   
    
    func getColor(_ row: Int) -> UIColor? {
        
        if row == 0 {
            return UIColor(red: CGFloat((47 / 255.0)), green: CGFloat((188 / 255.0)), blue: CGFloat((156 / 255.0)), alpha: 1.0)
        }
        else if row == 1 {
            return UIColor(red: CGFloat((156 / 255.0)), green: CGFloat((85 / 255.0)), blue: CGFloat((184 / 255.0)), alpha: 1.0)
        }
        else if row == 2 {
            return UIColor(red: CGFloat((229 / 255.0)), green: CGFloat((125 / 255.0)), blue: CGFloat((34 / 255.0)), alpha: 1.0)
        }
        else if row == 3 {
            return UIColor(red: CGFloat((232 / 255.0)), green: CGFloat((76 / 255.0)), blue: CGFloat((61 / 255.0)), alpha: 1.0)
        }
        else if row == 4 {
            return UIColor(red: CGFloat((57 / 255.0)), green: CGFloat((170 / 255.0)), blue: CGFloat((222 / 255.0)), alpha: 1.0)
        }
        else if row == 5 {
            return UIColor(red: CGFloat((52 / 255.0)), green: CGFloat((72 / 255.0)), blue: CGFloat((94 / 255.0)), alpha: 1.0)
        }
        else if row == 6 {
            return UIColor(red: CGFloat((173 / 255.0)), green: CGFloat((181 / 255.0)), blue: CGFloat((59 / 255.0)), alpha: 1.0)
        }
        else if row == 7 {
            return UIColor(red: CGFloat((138 / 255.0)), green: CGFloat((193 / 255.0)), blue: CGFloat((73 / 255.0)), alpha: 1.0)
        }
        else if row == 8 {
            return UIColor(red: CGFloat((234 / 255.0)), green: CGFloat((180 / 255.0)), blue: CGFloat((28 / 255.0)), alpha: 1.0)
        }
        else if row == 9 {
            return UIColor(red: CGFloat((255 / 255.0)), green: CGFloat((129 / 255.0)), blue: CGFloat((0 / 255.0)), alpha: 1.0)
        }
        
        return UIColor(red: CGFloat((57 / 255.0)), green: CGFloat((170 / 255.0)), blue: CGFloat((222 / 255.0)), alpha: 1.0)
        
    }
    
   
    
    
    func showActionSheet(_ vc: UIViewController!, title strTitle: String?, message msg: String?, listValues arrList: NSArray, completion: @escaping ActionSheetConfirmationBlock) {
        
        if arrList.count != 0 {
            
            let actionSheet = UIAlertController(title: strTitle, message: msg, preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            }))
            
            for i in 0..<arrList.count {
                actionSheet.addAction(UIAlertAction(title: "\(arrList[i])", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    var index = actionSheet.actions.index(of: action)
                    index = index! - 1
                    completion(index!)
                }))
            }
            
            actionSheet.popoverPresentationController?.permittedArrowDirections = []
            //For set action sheet to middle of view.
            var rect: CGRect = vc.view.frame
            rect.origin.x = vc.view.frame.size.width / 20
            rect.origin.y = vc.view.frame.size.height / 20
            actionSheet.popoverPresentationController?.sourceView = vc.view
            actionSheet.popoverPresentationController?.sourceRect = rect
            // Present action sheet.
            vc.present(actionSheet, animated: true) {() -> Void in }
    
        }
        
    }
    
    func currentCurrencySymbol(value: NSNumber, isNeedSymbol: Bool) -> String
    {
        
        let frLocale = Locale(identifier: "en-IN")
        
//        let formatter: NSNumberFormatter = {
//            let nf = NSNumberFormatter()
//            nf.numberStyle = .
//            nf.minimumFractionDigits = 0
//            nf.maximumFractionDigits = 1
//            return nf
//        }()
        
        let formatter = NumberFormatter()
        formatter.locale = frLocale
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: value) {
            
            if(isNeedSymbol)
            {
                return "\(formattedTipAmount)"
            }
            else
            {
                
                var newString = "\(formattedTipAmount)".replacingOccurrences(of: "₹ ", with: "")
                newString = "\(newString)".replacingOccurrences(of: "₹", with: "")

                return "\(newString)"

            }
            
        }
        
        return "0.0"
        
    }
    
    func currentCurrencySymbolWithdecimal(value: String, isNeedSymbol: Bool, decimalPlace: Int) -> String
    {
        
        let frLocale = Locale(identifier: "en-IN")
    
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = decimalPlace
        formatter.maximumFractionDigits = decimalPlace
        formatter.locale = frLocale
        formatter.numberStyle = .currency

//        let formattedTipAmount1 = formatter.string(from: Double(value)! as NSNumber)
//        print("values == ",formattedTipAmount1!)

        if let formattedTipAmount = formatter.string(from: Double(value)! as NSNumber) {
            
            if(isNeedSymbol)
            {
                return "\(formattedTipAmount)"
            }
            else
            {
                
                var newString = "\(formattedTipAmount)".replacingOccurrences(of: " ", with: "")
                newString = "\(newString)".replacingOccurrences(of: "₹", with: "")
                newString = "\(newString)".replacingOccurrences(of: "₹ ", with: "")

                return "\(newString)"
                
            }
            
        }
        
        return "0.0"
        
    }
    
    func stringFromHtml(string: String) -> NSAttributedString? {
        
            let attrStr = try! NSAttributedString(
                data: string.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [.documentType: NSAttributedString.DocumentType.html,
                          .characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)

            return attrStr
        
    }
    
    
    
  /*  func showNoRightsText(_ msg: String)
    {
        
       hideNoRightsText()
        
        var strMsg = msg
        
        if(strMsg == "")
        {
            
            strMsg = "You don't have rights to approval"
            
        }
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        if let aWidth = currentWindow?.frame.size.width, let aHeight = currentWindow?.frame.size.height {
            blurNoRightsView = UIView(frame: CGRect(x: 0, y: 70, width: aWidth, height: aHeight))
        }
        blurNoRightsView.backgroundColor = UIColor.white
        blurNoRightsView.alpha = 1.0
        currentWindow?.addSubview(blurNoRightsView)
        blurNoRightsView.isHidden = false
       
        
        lblNoRightsText = UILabel()
        if let aWidth = currentWindow?.frame.size.width {
            lblNoRightsText.frame = CGRect(x: 0, y: (currentWindow?.center.y)! + 55.0, width: aWidth, height: 30)
        }
        lblNoRightsText.text = strMsg
        lblNoRightsText.textColor = UIColor.darkGray
        
        lblNoRightsText.textAlignment = .center
        lblNoRightsText.font = UIFont(name: "Helvetica Bold", size: 16)
        lblNoRightsText.isHidden = false
        currentWindow?.addSubview(lblNoRightsText)
        
    }
    
    func hideNoRightsText()
    {
        
        blurNoRightsView.isHidden = true
        lblNoRightsText.isHidden = true
        
    }*/
    
    func convertObjectToString(_ data:Any) -> String
    {
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            
            return NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
        } catch let error as NSError {
            print(error)
        }
        
        return ""
    }

    func convertStringToDict(_ jsonText:String) -> NSDictionary
    {
        
        var dictonary:NSDictionary?
        
        if let data = jsonText.data(using: String.Encoding.utf8) {
            
            do {
                dictonary =  try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                
                if let myDictionary = dictonary
                {
                    
                    return myDictionary
                }
            } catch let error as NSError {
                print(error)
            }
        }
        
        return dictonary!
    }
    
    func convertStringToArray(_ jsonText:String) -> NSArray
    {
        
        var array:NSArray?
        
        if let data = jsonText.data(using: String.Encoding.utf8) {
            
            do {
                array =  try JSONSerialization.jsonObject(with: data, options: []) as? NSArray
                
                if let myArray = array
                {
                    
                    return myArray
                }
            } catch let error as NSError {
                print(error)
            }
        }
        
        return array!
    }
    
    func fourDecimalPlaces(_ value:Float) -> String
    {
        
        return String(format: "%.4f", value)

    }
    
    //MARK: Delate Success Status Rows in pendiong Approval DB:
    
 
    func emptyHyphenReplace(_ strText:String) -> String
    {
        
        if(strText.characters.count == 0)
        {
            
            return "-"
            
        }
        
        if(strText == "null")
        {
            
            return "-"
            
        }
        
        if(strText == "0-")
        {
            
            return "-"
            
        }
        
        return strText
    }
    
    func getVisibleValues(_ str:String) ->String
    {
        
        var commaActualObj = Common.shared.currentCurrencySymbolWithdecimal(value: "\(str)", isNeedSymbol: false, decimalPlace: 4)
        
        if(Double("\(str)") == 0.0)
        {
            
            commaActualObj = "-"
        }
        
        return commaActualObj
        
    }
    
    func removeNullValues(_ str: String?) -> String? {
        
        var strValues = str
        
        if ("\(str ?? "")" == "<null>") || ("\(str ?? "")" == "") || ("\(str ?? "")" == "null") {
            
            strValues = ""
        }
        
        return strValues
    }

    
}


//
//  URLConstants.swift
//  AppEngine
//
//  Created by Sandy on 12/10/18.
//  Copyright © 2018 Kamaraj. All rights reserved.
//

import Foundation

struct APP
{
    private struct Domains
    {
        static let UAT = "http://visconsultants.in/projects/appengine/instructions.php"
        static let Live = ""
        static let Local = ""
        static let QA = ""
    }
    
    private  static let BaseURL = Domains.UAT
    static var AppEngineBaseURL: String
    {
        return BaseURL
    }
    
}

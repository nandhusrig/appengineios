//
//  ViewController.swift
//  InshortsApp
//
//  Created by web on 02/11/18.
//  Copyright © 2018 InshortsApp. All rights reserved.
//

import UIKit

class DialogView: UIView {

    @IBOutlet var containerView: UIView!
    var containerWidth: CGFloat!
    var containerHeight: CGFloat!
    var containerX: CGFloat!
    var containerY: CGFloat!
    var dictSlides: NSDictionary!

    @IBOutlet var ivNewsImg: UIImageView!
    @IBOutlet var tvContent: UITextView!
    @IBOutlet var tvTitle: UILabel!
    @IBOutlet var btnTapToRead: UIButton!

    @IBOutlet weak var constImgHeight: NSLayoutConstraint!
    var vc: UIViewController!
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
      //  containerView.frame = CGRect(x: containerX, y: containerY, width: containerWidth, height: containerHeight)
//        containerView.translatesAutoresizingMaskIntoConstraints = true
        
        print("dictSlides == ",dictSlides)
        
        self.tvContent.text = self.removeNullValues("\(self.dictSlides.value(forKey: "PopupTextHTML") ?? "")")
        self.tvTitle.text = self.removeNullValues("\(self.dictSlides.value(forKey: "PopupHeader") ?? "")")
        
        
        let strImageUrl = self.removeNullValues("\(self.dictSlides.value(forKey: "PopupImage") ?? "")")
        
        if(strImageUrl != "")
        {
            
            self.constImgHeight.constant = 250.0

            let url = URL(string: strImageUrl ?? "")
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityIndicator.frame = self.ivNewsImg.bounds
            self.ivNewsImg.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            self.ivNewsImg.sd_setImage(with: url, completed: { image, error, cacheType, url in
                activityIndicator.removeFromSuperview()
                
                if(image != nil){
                    self.ivNewsImg.image = image
                }
                else
                {
                    self.ivNewsImg.image = UIImage(named: "noimage")
                    
                }
            })
            
        }
        else
        {
            
            self.constImgHeight.constant = 0.0
        }
        
    }
    
    func removeNullValues(_ str: String?) -> String? {
        
        var strValues = str
        
        if ("\(str ?? "")" == "<null>") || ("\(str ?? "")" == "") || ("\(str ?? "")" == "null") {
            
            strValues = ""
        }
        
        return strValues
    }

    
    @IBAction func btnTapToReadAction(_ sender: UIButton) {

     
        
    }

}






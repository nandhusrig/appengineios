//
//  ViewController.swift
//  SlideView
//
//  Created by Septiyan Andika on 6/26/16.
//  Copyright © 2016 sailabs. All rights reserved.
//

import UIKit

class DetailPageDialogVC: UIViewController {
    @IBOutlet weak var viewPager: ViewPager!

    var dictDisplay = NSDictionary()
    var arrSlides = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewPager.dataSource = self;
        // Do any additional setup after loading the view, typically from a nib.
//        viewPager.animationNext()
        
        dictDisplay = Common.shared.getLocal(key: "Display") as! NSDictionary
        
        arrSlides = dictDisplay.value(forKey: "Slides") as! NSArray
        
        viewPager.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        viewPager.scrollToPage(index: 0)
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension DetailPageDialogVC:ViewPagerDataSource
{
    
    func numberOfItems(viewPager:ViewPager) -> Int {
        return arrSlides.count;
        
    }
    
    func viewAtIndex(viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        var newView = view;
        
        let dictSlides = arrSlides[index] as! NSDictionary;

//        var label:UILabel?
        if(newView == nil){
//            newView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height:  self.view.frame.height))
//             newView!.backgroundColor = .randomColor()
//
//            label = UILabel(frame: newView!.bounds)
//            label!.tag = 1
//            label!.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
//            label!.textAlignment = .center
//            label!.font =  label!.font.withSize(28)
//            newView?.addSubview(label!)
            
            newView = Bundle.main.loadNibNamed("DialogView", owner: self, options: nil)?[0] as? UIView
            
            let v = newView as? DialogView
            v?.vc = self
//            v?.backgroundColor = UIColor.black
            v?.dictSlides = dictSlides

            
        }else{
//          label = newView?.viewWithTag(1) as? UILabel
            

        }
       
//        label?.text = "Page View Pager  \(index+1)"
        
        return newView!
    }
    
    func didSelectedItem(index: Int) {
        print("select index \(index)")
    }
   
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}


extension UIColor {
    static func randomColor() -> UIColor {
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}

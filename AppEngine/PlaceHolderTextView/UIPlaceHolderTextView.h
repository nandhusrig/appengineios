//
//  UIPlaceHolderTextView.h
//  TagTextField
//
//  Created by web on 23/09/17.
//  Copyright © 2017 GuruInfoTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPlaceHolderTextView : UITextView


@property (nonatomic, retain) IBInspectable NSString *placeholder;
@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@end

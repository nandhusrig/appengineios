//
//  HomeViewController.swift
//  AppEngine
//
//  Created by Sandy on 26/09/18.
//  Copyright © 2018 Kamaraj. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UIAlertViewDelegate,UITextViewDelegate
{
    @IBOutlet weak var btn_dgrEvnt: UIButton!
    @IBOutlet weak var btn_clrLog: UIButton!
    @IBOutlet weak var btn_AppModule: UIButton!
    @IBOutlet weak var btn_Event: UIButton!
    @IBOutlet weak var txtvClLog: UITextView!
    
    @IBOutlet weak var on_offSwt: UISwitch!
    var OnOffSwitchIsOn:Bool =  false
    
    var NetworkArray : NSMutableArray = NSMutableArray()

    var arrLogs = NSMutableArray()
    var arrActionCondition = NSMutableArray()
    var dictMainValues = NSMutableDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btn_dgrEvnt.layer.cornerRadius = 5
        btn_clrLog.layer.cornerRadius = 5
        
        txtvClLog!.autocorrectionType = UITextAutocorrectionType.no
        txtvClLog!.text = "Log Text Gose here..."
        txtvClLog!.textColor = UIColor.lightGray
        
        btn_AppModule.setTitle("No Module Selected", for: UIControlState.normal)
        btn_Event.setTitle("No Event Selected", for: UIControlState.normal)
        
        gedDatasFromWeb()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionAppModule(_ sender: Any)
    {
        let alert = UIAlertController(title: "App Module", message: "Select App Module", preferredStyle: UIAlertControllerStyle.alert)
        
        if !self.on_offSwt.isOn
        {
            for i in ["No Module Selected"] {
                alert.addAction(UIAlertAction(title: i, style: .default, handler: moduleListAction)
                )}
        }
        else
        {
            for i in ["No Module Selected","Networking Guide"] {
                alert.addAction(UIAlertAction(title: i, style: .default, handler: moduleListAction)
                )}
        }

        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionEvent(_ sender: Any)
    {
        let alert = UIAlertController(title: "Events", message: "Select Event", preferredStyle: UIAlertControllerStyle.alert)
        
        if btn_AppModule.titleLabel?.text=="No Module Selected"
        {
            for i in ["No Event Selected"] {
                alert.addAction(UIAlertAction(title: i, style: .default, handler: eventListAction)
                )}
        }
        else
        {
            alert.addAction(UIAlertAction(title: "No Event Selected", style: .default, handler: eventListAction))
            
            for i in NetworkArray
            {
                alert.addAction(UIAlertAction(title: i as! String, style: .default, handler: eventListAction))
            }
            
            /*alert.addAction(UIAlertAction(title: "Hi", style: .default, handler: eventListAction))
            
            for i in ["No Event Selected","RANDOM","TIME_ELAPSED_10S","TIME_ELAPSED_30S","TIME_ELAPSED_60S","SEARCH_EVENTS","APP_OPENED","SKILL_MATCH_NEW_ENTRY","SKILL_MATCH_EDIT_ENTRY","SKILL_MATCH_SUCCESSFUL","CONTACT_DETAIL_CLICKED","PIM_CONTACT_DELETED"] {
                alert.addAction(UIAlertAction(title: i, style: .default, handler: eventListAction)
                )}*/
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func moduleListAction(action: UIAlertAction)
    {
        //Use action.title
        btn_AppModule.setTitle(action.title, for: UIControlState.normal)
        
        if action.title == "No Module Selected"
        {
            btn_Event.setTitle("No Event Selected", for: UIControlState.normal)
        }
        else
        {
        }
    }
    
    func eventListAction(action: UIAlertAction)
    {
        //Use action.title
        btn_Event.setTitle(action.title, for: UIControlState.normal)
    }
    
    @IBAction func switchChanged(_ sender: UISwitch)
    {
        valueChange(mySwitch: on_offSwt)
    }
    
    @objc func valueChange(mySwitch: UISwitch)
    {
        if mySwitch.isOn
        {
            OnOffSwitchIsOn = true
        }
        else
        {
            OnOffSwitchIsOn = false
            btn_AppModule.setTitle("No Module Selected", for: UIControlState.normal)
            btn_Event.setTitle("No Event Selected", for: UIControlState.normal)
        }
    }
    
    func gedDatasFromWeb()
    {
        let url = URL(string: APP.AppEngineBaseURL)
        
        let task = URLSession.shared.dataTask(with: url! as URL) { data, response, error in
            guard let data = data, error == nil else { return }
            
//            print(NSString(data: data, encoding: String.Encoding.utf8.rawValue) ?? 0)
            
            //let datastring = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            do{
                let json = try JSONSerialization.jsonObject(with: data, options:[]) as! [[String:Any]]
                
                self.NetworkArray.removeAllObjects()
                for obj in json
                {
//                    print(obj)
                    if let aCon = obj["ActionCondition"]
                    {
//                        print(aCon)
                        if let rootDictionary = aCon as? [String: Any] {
                            let name = rootDictionary["0"]
                            var fVal : [Any] = name as! [Any]
//                            print(fVal[0])
                            self.NetworkArray.addObjects(from: [fVal[0]])
                        }
                    }
                }
                print("self.NetworkArray == ",self.NetworkArray)
                
                for obj in json
                {

                    let dictDisplay = obj["Display"] as! NSDictionary
                    let tempDictActionCondition = obj["ActionCondition"] as! NSDictionary

                    for index in 0..<tempDictActionCondition.allKeys.count
                    {

                        let strKey =  "\(tempDictActionCondition.allKeys[index])"
                        let arrSplit = tempDictActionCondition.value(forKey: strKey) as! NSArray
                        let strSplited = arrSplit.componentsJoined(by: ",")
                        self.dictMainValues.setValue(dictDisplay, forKey: strSplited)

                    }

                }
                
                print("dictMainValues == ",self.dictMainValues)

                
            }
            catch {
                print("Error with Json: \(error)")
            }

        }
        
        task.resume()
        
        
        /*WebRqt.getDataFromServer_using_MethodGET(url: fullURL, completion: { response in
            print("response :", response)
            let ActionCondition = response["ActionCondition"]
            print("ActionCondition :", ActionCondition ?? 0)
            
            //let full_day_present = response["full_day_present"]
            //let half_day_present = response["half_day_present"]
        })*/
    }
    
    
    //MARK: - UITextView delegate methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (txtvClLog?.text == "Log Text Gose here...")
        {
            txtvClLog!.text = nil
            txtvClLog!.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtvClLog!.text.isEmpty
        {
            txtvClLog!.text = "Log Text Gose here..."
            txtvClLog!.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - Button Actions::

    
    @IBAction func btnClearLogsAction(_ sender: UIButton) {
        
        arrLogs.removeAllObjects()
        arrActionCondition.removeAllObjects()
        self.txtvClLog.text = ""
        
    }
    
    @IBAction func btnTriggerAction(_ sender: UIButton) {
        
        if(btn_AppModule.titleLabel?.text == "No Module Selected" || btn_Event.titleLabel?.text == "No Event Selected" )
        {
            return;
        }
        
        let strAppend = self.getCurrentDate() + "  " + (btn_Event.titleLabel?.text)!
        
        arrLogs.add(strAppend)
        
        arrActionCondition.add((btn_Event.titleLabel?.text)!)
        
        let arrSplitedValue = arrLogs.componentsJoined(by: "\n")
        
        self.txtvClLog.text = arrSplitedValue

        let strActionCondition = arrActionCondition.componentsJoined(by: ",")
        
        let dictDisplay = dictMainValues.value(forKey: strActionCondition)
        
        print("dictDisplay == ",dictDisplay)
        
        if(dictDisplay != nil)
        {
           
            Common.shared.saveLocal(value: dictDisplay as Any, key: "Display")
            openDialogView()
            
        }
        
    }
    
    func openDialogView()
    {
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//
//        let viewController = story.instantiateViewController(withIdentifier: "DetailPageDialogVC") as! DetailPageDialogVC
//
//        let nav = UINavigationController(rootViewController: viewController)
//
//        viewController.view.backgroundColor = UIColor.clear
//
//        nav.isNavigationBarHidden = true
//
//        self.present(nav, animated: false, completion: nil)
        
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "DetailPageDialogVC") as? DetailPageDialogVC
        vc?.view.backgroundColor = UIColor.clear;
        vc?.modalPresentationStyle = .overCurrentContext;
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    func getCurrentDate() -> String {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let result = formatter.string(from: date)
        return result
    }
    
}
